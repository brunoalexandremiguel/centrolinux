---
title: "Agenda"
date: 2023-09-T21:05:11-00:00
---

# Próxima(s) actividade(s)

A(s) próxima(s) actividade(s) do Centro Linux é/são:

[![Evento de Fevereiro](https://centrolinux.pt/images/2024-02.Fev2024/cartaz_fev2024.png)](https://centrolinux.pt/post/2024-fevereiro/)


## Esta é a lista completa actividades do Centro Linux.

*(Inclui actividades anteriores)*
