---
date: 2024-02-28
description: "Evento do Centro Linux, de Fevereiro, de 2024"
featured_image: ""
tags: ["2024", "Git", "GitLab", "GrupoDeEstudo", "Home Assistant", "NAS"]
title: "Evento de Fevereiro de 2024"
---

[![Cartaz](https://centrolinux.pt/images/2024-02.Fev2024/cartaz_fev2024.png)](https://osm.org/go/b5crq_xVM?layers=N)


# Evento de Fevereiro

Neste mês de Fevereiro (dia 17) o evento do Centro Linux será dedicado às seguintes actividades:

 * Primeira Sessão do Grupo de Estudo do Centro Linux (curso de introdução ao [gitlab](https://www.gitlab.com/) com [git](https://git-scm.com/)).

 * Introdução ao [Home Assistant](https://www.home-assistant.io/).

 * Investigação de armazenamento em rede ([openmediavault](https://www.openmediavault.org/) [TrueNAS Scale](https://www.truenas.com/download-truenas-scale/)).

O git é uma ferramenta colaborativa essencial para quem contribui para o Software Livre e Open Source Software, ou o quer utilizar de uma forma mais avançada, e o GitLab é uma plataforma online colaborativa baseada em git. O Grupo de Estudo do Centro Linux, vai debruçar-se sobre este tema durante os próximos dois meses.
Venham adquirir um conjunto de competências muito úteis!

O Home Assistant é uma plataforma de automação desenhada para a Internet das Coisas. É Software Livre e Open Source Software, e desenhada para podermos utilizar a Internet das coisas de forma mais independente, aberta, privada, e segura.
Se querem introduzir-se a esta plataforma, esta é uma boa oportunidade!

Por vezes precisamos de armazenar e partilhar dados entre diversos computadores, ou temos computadores com pouca capacidade de armazenagem e de expansão dessa capacidade de armazenagem, o armazenamento em rede, é a forma mais simples de resolver esse problema (entre outros). Vamos investigar as soluções openmediavault e se truenas.
Juntem-se a nós enquanto exploramos várias opções!


## Quando?

No **Sábado dia 17 de Fevereiro de 2024 a partir das 11:30**
Haverá interrupção das actividades para almoço.

### Plano (provisório) de actividades:

#### Agenda

##### Manhã

| Hora  | Descrição                                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 10:00 | Pequeno-almoço linuxeiro *([detalhes aqui](https://centrolinux.pt/pequeno-almoco-linuxeiro/))* |
| 11:00 | Chegada dos voluntários ao Centro Linux |
| 11:05 | Montagem do espaço para as actividades |
| 11:30 | Actividade do Grupo de Estudo |
| 13:00 | Interrupção para o almoço |

##### Tarde
| Hora  | Descrição |
| ----- | ------------------------------------------------------------------- |
| 15:00 | Introdução ao Home Assistant |
| 16:30 | Pausa para lanche |
| 17:00 | Armazenamento em rede (openmediavault e TrueNas Scale) |
| 19:00 | Desmontagem e fim das actividades  |

### Material necessário

O Centro Linux tem algum do material necessário que disponibilizaremos para os participantes, contudo podemos não ter material para todos dependendo da quantidade de participantes. Assim sendo, se for possível recomendamos que tragam um computador portátil.

### Local:

O Workshop será realizado no Centro Linux que se localiza no Makers In Little Lisbon - MILL, que é em Lisboa, muito perto do Campo dos Mártires da Pátria.
Pode consultar [onde estamos](https://centrolinux.pt/ondeestamos/) para morada e informações detalhadas sobre como pode chegar ao MILL de muitas formas diferentes.

### Inscrições

Podem inscrever-se na [página de inscrições](https://mill.pt/agenda/centro-linux-encontro-de-fevereiro/).

### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.
