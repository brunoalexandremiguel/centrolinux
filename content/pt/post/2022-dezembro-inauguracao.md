---
date: 2022-10-22
description: "A actividade inaugural do Centro Linux"
featured_image: ""
tags: []
title: "A inauguração"
---

[![Cartaz](https://centrolinux.pt/images/Inauguracao/cartaz_twitter.png)](https://osm.org/go/b5crq_xVM?layers=N)


## Inauguração do Centro Linux

Esta actividade do [Centro Linux](https://centrolinux.pt) decorre no dia 22 de Outubro, pelas 15:30, no [Makers in Little Lisbon](https://mill.pt/), a casa do Centro Linux.

O evento é simultaneamente uma Festa de Lançamento do Ubuntu 22.10 Kinetic Kudu e os participantes terão a oportunidade de instalar, e experimentar esta nova versão de [Ubuntu](https://www.ubuntu.com) nos computadores do Laboratório do Centro Linux.

Se tiverem computadores portáteis nos quais pretendam experimentar o Ubuntu 22.10 Kinetic Kudu (instalando no computador, ou não), também facilitaremos essa oportunidade se nos for possível tendo em conta a capacidade momentânea de manter o resto do evento a decorrer.


> A Participação é gratuita mas requer inscrição, porque o espaço tem limites de capacidade e precisamos de fazer gestão dessa capacidade

**Inscrições**

[Seguindo este link para o site do MILL](https://mill.pt/agenda/a-actividade-inaugural-do-centro-linux/)

A agenda detalhada do evento consiste de:

| Hora | Descrição |
| ---- | ------ |
| 15:30 | Boas-vindas aos convidados e participantes nas actividades, e curta apresentação do Centro Linux |
| 15:45 | Apresentação do [Makers in Little Lisbon](https://mill.pt) aos convidados |
| 16:00 | Apresentação da Comunidade Ubuntu Portugal |
| 16:15 | Pausa técnica |
| 16:30 | Início das actividades práticas:
| > | 1. Primeira montagem dos computadores e rede do laboratório do Centro Linux |
| > | 2. Instalação e experimentação com o sistema operativo Ubuntu 22.10 Kinetic Kudu nos computadores.
| > | 3. Repetição do ponto 2 por todos os que queriam experimentar o novo Ubuntu 22.10, ou simplesmente aprender a instalar Ubuntu.
| 18:30 | Palavras finais da organização |
| 19:00 | Desmontagem, arrumação e limpeza do espaço |
| 19:30 | Jantar-convivío (em lugar perto ainda da determinar) |

### Localização:

MILL – Makers In Little Lisbon
Calçada do Moinho de Vento, 14B,
1150-236 Lisboa

coordenadas: 38.72045,-9.14131?z=19

Localização do MILL no mapa do [OpenStreetMap](https://osm.org/go/b5crq_xVM?layers=N):

[![Localização do MILL](https://centrolinux.pt/images/map.png "mapa com a localização do MILL, clique para mais detalhes e navegação")](https://osm.org/go/b5crq_xVM?layers=N)
