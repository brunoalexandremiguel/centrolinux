---
title: Temas do Centro Linux
description: Os temas abordados com frequência no Centro Linux
featured_image: ''
omit_header_text: false
type: page
menu: main
---

Os temas até agora mais abordados no Centro Linux são:

* [Ubuntu](https://centrolinux.pt/ubuntu)
* [Linux](https://centrolinux.pt/linux)